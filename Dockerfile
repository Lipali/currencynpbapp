FROM adoptopenjdk:11-jre-hotspot
ADD target/currencyNBPApi-0.0.1-SNAPSHOT.jar .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "currencyNBPApi-0.0.1-SNAPSHOT.jar"]
