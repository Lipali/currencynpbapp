package com.example.currencyNBPApi;

import com.example.currencyNBPApi.model.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<Log, Long> {

}
