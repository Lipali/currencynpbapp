package com.example.currencyNBPApi.model;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class RateDto {
    private String currency;
    private String code;
    private BigDecimal bid;
    private BigDecimal ask;
}
