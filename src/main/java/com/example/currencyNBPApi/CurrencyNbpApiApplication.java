package com.example.currencyNBPApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CurrencyNbpApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CurrencyNbpApiApplication.class, args);
	}

}
