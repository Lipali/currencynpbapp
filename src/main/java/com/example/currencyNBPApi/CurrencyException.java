package com.example.currencyNBPApi;

public class CurrencyException extends Exception{
    public CurrencyException() {}
    public CurrencyException(String s){
        super(s);
    }
}
