package com.example.currencyNBPApi;

import com.example.currencyNBPApi.model.AnswerDto;
import com.example.currencyNBPApi.model.Log;
import com.example.currencyNBPApi.model.RateDto;
import com.example.currencyNBPApi.model.TechDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class RateService {

    private final RestTemplate restTemplate= new RestTemplate();
    final LogRepository logRepository ;
    private final static String CURRENCY_EXCHANGE_RATES_TABLE = "http://api.nbp.pl/api/exchangerates/tables/c?format=json";

    public List<RateDto> getAllRates() throws CurrencyException {
        String message = "get all rates -api connection";
        saveLogs(message);
        log.info(message);

        TechDto[] techDtos = restTemplate.getForObject(CURRENCY_EXCHANGE_RATES_TABLE, TechDto[].class);
        if (techDtos==null){
            message="comunication error";
            throw new CurrencyException(message);
        }
        return techDtos[0].getRates();
    }
    public List<String> getAllRatesCurrency() throws CurrencyException {
        List<RateDto>rates = getAllRates();
        String message = "get all rates currency";
        saveLogs(message);
        log.info(message);

        List<String> result = new LinkedList<>();
        for(RateDto rate : rates){
            String temp =rate.getCurrency();
            result.add(temp);
        }
        return result;
    }

    public AnswerDto buyCurrency(String code , BigDecimal amount) throws CurrencyException {
        String message = "buy currency"+" "+code+" "+amount;
        saveLogs(message);
        log.info(message);

        BigDecimal result = null;
        List<RateDto> rates = getAllRates();
        if ((codeValidator(code,rates)==true)&&(amountValidator(amount)==true)) {
            for (RateDto temp : rates) {
                if (temp.getCode().equals(code)) {
                    result = amount.divide(temp.getAsk(), 2, RoundingMode.HALF_UP);
                }
            }
        }
        else{
            message ="invalid parameters";
            throw new CurrencyException(message);}
        return new AnswerDto(result);
    }
    public AnswerDto sellCurrency(String code , BigDecimal amount) throws CurrencyException {
        String message = "sell currency"+" "+code+" "+amount;
        saveLogs(message);
        log.info(message);

        List<RateDto> rates = getAllRates();
        BigDecimal result = null;
        if ((codeValidator(code,rates)==true)&&(amountValidator(amount)==true)) {
            for (RateDto temp : rates) {
                if (temp.getCode().equals(code)) {
                    result = temp.getBid().multiply(amount);
                }
            }
            result = result.setScale(2, RoundingMode.HALF_UP);
        }
        else{
            message ="invalid parameters";
            throw new CurrencyException(message);
        }
        return new AnswerDto(result);
    }

    public Boolean codeValidator(String code,List<RateDto>rates){
        boolean flag = false;
        for (RateDto temp :rates){
            if (temp.getCode().equals(code)){
                flag =true;
                break;
            }
        }
        return flag;
    }

    public boolean amountValidator(BigDecimal amount){
        if(amount.doubleValue()>=0.01)
            return true;
        else
            return false;
    }
    public void saveLogs(String message){
        Log log = Log.builder()
                .message(message)
                .time(LocalDateTime.now())
                .build();
        logRepository.save(log);
    }

}
