package com.example.currencyNBPApi;


import com.example.currencyNBPApi.model.AnswerDto;
import com.example.currencyNBPApi.model.RateDto;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class RateController {

    private final RateService rateService;

    @CrossOrigin
    @GetMapping("/api")
    public List<String> getCurrency() throws CurrencyException {
        return rateService.getAllRatesCurrency();
    }

    @CrossOrigin
    @GetMapping("/api/currency")
    public List<RateDto> getAllRates() throws CurrencyException {
        return rateService.getAllRates();
    }

    @CrossOrigin
    @GetMapping("/api/currency/buy")
    public AnswerDto buyCurrency(@RequestParam("code") String code, @RequestParam("amount") BigDecimal amount) throws CurrencyException {
        return rateService.buyCurrency(code,amount);
    }

    @CrossOrigin
    @GetMapping("/api/currency/sell")
    public AnswerDto sellCurrency(@RequestParam("code") String code, @RequestParam("amount") BigDecimal amount) throws CurrencyException {
        return rateService.sellCurrency(code,amount);
    }
}
