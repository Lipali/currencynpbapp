package com.example.currencyNBPApi;

import com.example.currencyNBPApi.model.Log;
import com.example.currencyNBPApi.model.RateDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.FluentQuery;
import org.testng.annotations.Ignore;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;

@DataJpaTest
public class RateServiceTest {

    @Autowired
    LogRepository logRepository;
    @Autowired
    RateService rateService = new RateService(logRepository);


    @Test
    public void codeValidator_shouldReturnTrueWhenUSD() {

        //given:
        RateDto rateA = RateDto.builder()
                .currency("dolar amerykański")
                .code("USD")
                .bid(BigDecimal.valueOf(4.4044))
                .ask(BigDecimal.valueOf(4.4934))
                .build();
        RateDto rateB = RateDto.builder()
                .currency("euro")
                .code("EUR")
                .bid(BigDecimal.valueOf(4.6391))
                .ask(BigDecimal.valueOf(4.7329))
                .build();
        List<RateDto> list = new LinkedList<>();
        list.add(rateA);
        list.add(rateB);

        boolean exeptedValue = true;
        //when
        Boolean  result = rateService.codeValidator("USD",list);

        //then
        Assertions.assertEquals(exeptedValue, result);
    }
    @Test
    public void codeValidator_shouldReturnFalseWhenRRR() {

        //given:
        RateDto rateA = RateDto.builder()
                .currency("dolar amerykański")
                .code("USD")
                .bid(BigDecimal.valueOf(4.4044))
                .ask(BigDecimal.valueOf(4.4934))
                .build();
        RateDto rateB = RateDto.builder()
                .currency("euro")
                .code("EUR")
                .bid(BigDecimal.valueOf(4.6391))
                .ask(BigDecimal.valueOf(4.7329))
                .build();
        List<RateDto> list = new LinkedList<>();
        list.add(rateA);
        list.add(rateB);

        boolean exeptedValue = false;
        //when
        Boolean  result = rateService.codeValidator("RRR",list);

        //then
        Assertions.assertEquals(exeptedValue, result);
    }
    @Test
    public void amountValidator_shouldReturnFalseWhenMinus() {
        //given:
        BigDecimal testValue = BigDecimal.valueOf(-3);
        boolean exeptedValue = false;

        //when

        Boolean  result = rateService.amountValidator(testValue);

        //then
        Assertions.assertEquals(exeptedValue, result);

    }
    @Test
    public void amountValidator_shouldReturnTrueWhenGoodValue() {
        //given:
        BigDecimal testValue = BigDecimal.valueOf(3.44);
        boolean exeptedValue = true;

        //when
        Boolean  result = rateService.amountValidator(testValue);

        //then
        Assertions.assertEquals(exeptedValue, result);

    }



}